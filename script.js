/*test*/
/* console.log("Javascript"); */

/* create timer object */
let timerObj={
  minutes:0,
  seconds:0,
  timerId:0
}

/* create function to activate audio sound/alarm */
function soundAlarm(){
  /*amount of times the timer will activate */
  let amount=3;
  let audio=new Audio("Timer_Sound_Effect.mp3");
  /*audio.play();*/
  function playSound(){
    audio.pause();
    audio.currentTime=0;
    audio.play();
  }
  for (let i = 0; i < amount; i++) {
    setTimeout(playSound,1200*i);
  }
}

function updateValue(key,value){
  if (value<0) {
    value=0;
    console.log("positive values only");
  }
  if (key=="seconds") {
    if (value<10) {
      value="0"+ value;
    }
    if(value > 60){ value=59;}
  }
  /* passing id to key and make it equal to value or 0 */
  /* key to identify if we are setting seconds or minutes so we don't have to make two functions to upadte their values */
  $("#"+key).html(value || 0);
  timerObj[key]=value;

  console.log("min",timerObj.minutes);
  console.log("sec",timerObj.seconds);
}

/*console.log(timerObj[2]=3);*/
/*updateValue(#seconds,12);

/* a function that is declared and called at the same time */
(function detectChanges(key){
  console.log("detect changes");
  /* input= id(minutes-input)or id(seconds-input) selon key */
  let input="#"+key+"-input";

  /* update value of minutes or seconds */
  /* $(selector).val(): get value of selected elt
     $(selector).val(val): set value to selected elt */
  $(input).change(function(){
    updateValue(key,$(input).val());
  });

  /* keyup: trigger keyup event (when a keyboard key is pressed)*/
  $(input).keyup(function(){
    updateValue(key,$(input).val());
  });

  return arguments.callee;
})("minutes")("seconds"); /* use function detectChanges twice for minutes and seconds */


/* button functions */
function startTimer(){
  /* when i click start button i need to disable it again and to activate the stop/pause button to press it when i need */
  buttonManager(["start",false],["pause",true],["stop",true]);
  freezingInputs();
  timerObj.timerId = setInterval(function() {
      timerObj.seconds--;
      if(timerObj.seconds < 0) {
          if(timerObj.minutes == 0) {
              soundAlarm();
              return stopTimer();
          }
          timerObj.seconds = 59;
          timerObj.minutes--;
      }

      updateValue("minutes", timerObj.minutes);
      updateValue("seconds", timerObj.seconds);
  }, 1000);
}


function stopTimer(){
  clearInterval(timerObj.timerId);
  /* when i click stop i need to renable start */
  buttonManager(["start",true],["pause",false],["stop",false]);
  unfreezingInputs();
  updateValue("minutes", $("#minutes-input").val());

    // Fixing the issue that happens when you stop the timer, and the timer will indicate something like. 2:0
    // This will make a timer correctly display the seconds in the correct format. 2:00
    let seconds = $("#seconds-input").val();
    if(seconds < 10) { seconds = "0" + seconds; }
    updateValue("seconds", seconds);
}


function pauseTimer(){
  /*when i click pause i need to renable start and stop buttons */
  buttonManager(["start",true],["pause",false],["stop",true]);
  clearInterval(timerObj.timerId);
}

/* ...buttonsArray: t5allini nda5el 9ad man7eb parametres ==> array f sortie */
/* principe::: ["start",true]==> enabling start button ...*/
function buttonManager(...buttonsArray){
  /* to test function console.log(buttonsArray);*/
  for(let i=0; i<buttonsArray.length; i++){
    /* 0==true: enabling
       1==false:disabling */
    let button="#"+buttonsArray[i][0]+"-button";
    if (buttonsArray[i][1]) {
      $(button).removeAttr('disabled');
    }
    else {
      /* set disabled attr to do=isabled weird but this is the syntax*/
      $(button).attr('disabled','disabled');
    }
  }

}

/* freezing minutes and seconds inputs when the counter is counting to not mess the counted value*/
function freezingInputs(){
  $("#seconds-input").attr('disabled','disabled');
  $("#minutes-input").attr('disabled','disabled');
}

/* enabling inputs after the counter stops counting */
function unfreezingInputs(){
  $("#seconds-input").removeAttr('disabled');
  $("#minutes-input").removeAttr('disabled');
}
